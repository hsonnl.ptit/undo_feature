using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace TN_CSDLPT
{
    public partial class frmChuanBiThi : DevExpress.XtraEditors.XtraForm
    {
		//first step: create an form UndoStack to contain the data and action.
        private Stack<UndoTarget> undoTarget = new Stack<UndoTarget>();
		
		//seccond step: create target to holding the table data
        class CBThi
        {
            public string MAGV, MAMH, MALOP, TRINHDO;
            public DateTime NGAYTHI;
            public short LAN, SOCAUTHI, THOIGIAN;
			//creating the CBThi constructor
            public CBThi(string MAGV, string MAMH, string MALOP, string TRINHDO, DateTime NGAYTHI, short LAN, short SOCAUTHI, short THOIGIAN)
            {
                this.MAGV = MAGV;
                this.MAMH = MAMH;
                this.MALOP = TRINHDO;
                this.TRINHDO = TRINHDO;
                this.NGAYTHI = NGAYTHI;
                this.LAN = LAN;
                this.SOCAUTHI = SOCAUTHI;
                this.THOIGIAN = THOIGIAN;
            }
        }
		//ex function.
        void InsertFormCBThi(string MAGV, string MAMH, string MALOP, string TRINHDO, DateTime NGAYTHI, short LAN, short SOCAUTHI, short THOIGIAN)
        {
			try{
				
				/*
				**inserting data to the table with arguments;
				*/
				CBThi data = new CBThi(MAGV, MAMH, MALOP, TRINHDO, NGAYTHI, LAN, SOCAUTHI, THOIGIAN);
				//create label to hold the action; in this case, we are going to insert an new data to CBThi table.
				String action = "insert";
				UndoTarget target = new UndoTarget(data, action);
				//ex:
				this.undoTarget.Push(target);//insert on top of the stack;StackSize++;
			}
        }

        private void btnPhucHoi_Click(object sender, EventArgs e)
        {
            if (this.undoTarget.Count <= 0)return;//empty stack
            UndoTarget popData = this.undoTarget.Pop();//return an undoTarget on top of Stack;StackSize--;
			string action = popData.HANHDONG;
			switch(action){
				case "insert":
					//call function delete data table; data = popData.data <== dynamic type
					break;
				case "delete":
					//call function insert data table
					break;
				case "update":
					//call function return data before update
					break;
				default:
					break;
			}
        }
		//form load
        private void frmChuanBiThi_Load(object sender, EventArgs e){}
    }
}